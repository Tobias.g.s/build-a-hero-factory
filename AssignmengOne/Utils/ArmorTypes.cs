﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Utils
{
    /// <summary>
    /// Types of armor.
    /// </summary>
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
