﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Utils
{
    internal enum HeroClasses
    {
        Mage,
        Warrior,
        Rogue,
        Ranger
    }
}
