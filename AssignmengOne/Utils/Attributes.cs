﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Utils
{
    /// <summary>
    /// The attributes of a hero, strength , dexterity and intelligence.
    /// </summary>
    /// 
    // I made the Attributes a struct instead of a class as it does not need to hold logic beyond data.
    public struct Attributes
    {
        public Attributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public override string ToString()
        {
            return "\tStr:" + Strength + "\tDex:" + Dexterity + "\tInt:" + Intelligence;
        }

        // overloading of the + operator to add each individual attributes to eachother.
        public static Attributes operator +(Attributes a, Attributes b)
        {
            return new Attributes(a.Strength + b.Strength, a.Dexterity + b.Dexterity, a.Intelligence + b.Intelligence);
        }
    }
}
