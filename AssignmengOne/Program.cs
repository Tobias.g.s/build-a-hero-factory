﻿using MyHero.Models.Heros.Classes;
using MyHero.Utils;
using MyHero.Models.Heros;
using System.Security.Cryptography.X509Certificates;
using MyHero.Models.Equipments;

internal class Program
{
    private static void Main(string[] args)
    {
        Warrior Bob = new Warrior("Bob");
        Console.WriteLine(Bob.ArmorSchool.Contains(ArmorTypes.Plate));
        Weapon sword = new Weapon("Sword", 1, new Attributes(0, 0, 0), 1.0, WeaponTypes.Sword);
        ArmorPiece helmet = new ArmorPiece("Helmet of doom", 1, Slots.Head, ArmorTypes.Plate, 2);
        ArmorPiece body = new ArmorPiece("body of doom", 1, Slots.Body, ArmorTypes.Plate, 2);
        ArmorPiece legs = new ArmorPiece("legs of doom", 1, Slots.Legs, ArmorTypes.Plate, 2);
        Bob.Equip(sword);
        Bob.Equip(helmet);
        Bob.Equip(body);
        Bob.Equip(legs);
        Bob.DisplayHero();
        
        
    }
}