﻿using MyHero.Models.Exceptions;
using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Equipments
{
    public class ArmorPiece : Equipment
    {
        public ArmorPiece(string name, int requiredLevel, Slots slot, Attributes attributes ,ArmorTypes armorType, double armorValue) : base(name, requiredLevel, slot, attributes)
        {
            ArmorType = armorType;
            ArmorValue = armorValue;
            if (Slot == Slots.Weapon)
            {
                throw new InvalidSlotException("A weapon can not be an ArmorPiece");
            }
        }
        // second constructer for statless equipments.
        public ArmorPiece(string name, int requiredLevel, Slots slot, ArmorTypes armorType, double armorValue) : base(name, requiredLevel, slot)
        {
            ArmorType = armorType;
            ArmorValue = armorValue;
            if (Slot == Slots.Weapon)
            {
                throw new InvalidSlotException("A weapon can not be an ArmorPiece");
            }
        }
        public ArmorTypes ArmorType { get; }
        public double ArmorValue { get;}
    }
}
