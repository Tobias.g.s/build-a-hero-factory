﻿using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Equipments
{
    public abstract class Equipment
    {

        public string Name { get; }
        public int RequiredLevel { get; }
        public Slots  Slot { get; }

        public Attributes Attributes { get; }

        public Equipment(string name, int requiredLevel, Slots  slot, Attributes attributes)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
            Attributes = attributes;
        }
        public Equipment(string name, int requiredLevel, Slots slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
            Attributes = new Attributes(0, 0, 0);
        }

    }
}
