﻿using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Equipments
{
    public class Weapon : Equipment
    {

        public double WeaponDam { get; }
        public WeaponTypes WeaponType { get; }

        public Weapon(string name, int requiredLevel, Attributes attributes, double weaponDam,WeaponTypes weaponType) : base(name, requiredLevel, Slots.Weapon, attributes)
        {
            WeaponDam = weaponDam;
            WeaponType= weaponType;
        }
        // second constructor for statless weapons.
        public Weapon(string name, int requiredLevel, double weaponDam, WeaponTypes weaponType) : base(name, requiredLevel, Slots.Weapon)
        {
            WeaponDam = weaponDam;
            WeaponType = weaponType;
        }
    }
}
