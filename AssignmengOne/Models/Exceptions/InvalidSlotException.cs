﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Exceptions
{
    /// <summary>
    /// Thrown when an eqiuipment has invalid slot for its type.
    /// </summary>
    public  class InvalidSlotException : Exception
    {
        public InvalidSlotException() { }

        public InvalidSlotException(string message) : base(message)
        {

        }
    }
}
