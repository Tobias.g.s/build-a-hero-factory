﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Exceptions
{
    /// <summary>
    /// Thrown when an invalid Armor Equipment is attepmted to be equipped.
    /// </summary>
    [Serializable]
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() { }

        public InvalidArmorException(string? message) : base(message)
        {
        }
    }
}
