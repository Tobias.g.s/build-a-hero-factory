﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Exceptions
{
    /// <summary>
    /// Thrown when a Hero attempts to equip an invalid Weapon, either non compatible weaponType or hero level is too low.
    /// </summary>
    [Serializable]
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() { }

        public InvalidWeaponException(string message) : base(message) { }


    }
}
