﻿using MyHero.Models.Equipments;
using MyHero.Models.Heros;
using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Heros.Classes
{
    internal class Ranger : Hero
    {
        public Ranger(string name) : base(
            name, 
            new List<ArmorTypes>() { ArmorTypes.Leather, ArmorTypes.Mail },
            new List<WeaponTypes>() { WeaponTypes.Bow }
            )
        {
            level = 1;
            attributes = new Utils.Attributes(1, 7, 1);
        }

        public override void LevelUp()
        {
            attributes.Strength += 1;
            attributes.Dexterity += 5;
            attributes.Intelligence += 1;
        }
        public override double DoDamage()
        {
            Weapon? weapon = (Weapon?)Equipped[Slots.Weapon];
            if (weapon != null)
            {
                return weapon.WeaponDam * (1 + (GetTotalAttributes().Dexterity / 100));
            }
            return 1 * (1 + GetTotalAttributes().Dexterity / 100);
        }
    }
}
