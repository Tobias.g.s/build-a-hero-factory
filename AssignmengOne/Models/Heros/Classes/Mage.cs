﻿using MyHero.Models.Equipments;
using MyHero.Models.Heros;
using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Heros.Classes
{
    /// <summary>
    /// A variation of Hero, mages focus on smartiness.
    /// </summary>
    public class Mage : Hero
    {
        public Mage(string name) : base(
            name, 
            new List<ArmorTypes>() { ArmorTypes.Cloth }, 
            new List<WeaponTypes>() { WeaponTypes.Staff, WeaponTypes.Wand }
            )
        {
            level= 1;
            attributes = new Utils.Attributes(1, 1, 8);
        }

        public override void LevelUp()
        {
            attributes.Strength += 1;
            attributes.Dexterity += 1;
            attributes.Intelligence += 5;
        }

        public override double DoDamage()
        {
            Weapon? weapon = (Weapon?)Equipped[Slots.Weapon];
            if (weapon != null)
            {
                return weapon.WeaponDam * (1 + (GetTotalAttributes().Intelligence / 100));
            }
            return 1 * (1 + GetTotalAttributes().Intelligence / 100);
        }
    }
}
