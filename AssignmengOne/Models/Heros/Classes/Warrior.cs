﻿using MyHero.Models.Equipments;
using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Heros.Classes
{
    /// <summary>
    /// Warrior hero strong.
    /// Warriors main stat is strenght.
    /// Warriors can only equip swords, hammers and axes as weapons and can only wear plate and mail armor.
    /// </summary>
    public class Warrior : Hero
    {
        public Warrior(string name) : base(
            name,
            new List<ArmorTypes>() { ArmorTypes.Plate, ArmorTypes.Mail },
            new List<WeaponTypes>() { WeaponTypes.Axe, WeaponTypes.Hammer, WeaponTypes.Sword }
            )
        {
            level = 1;
            attributes = new Attributes(5, 2, 1);
        }

        public override double DoDamage()
        {
            Weapon? weapon = (Weapon?)Equipped[Slots.Weapon];
            if (weapon != null)
            {
                return weapon.WeaponDam * (1 + GetTotalAttributes().Strength / 100);
            }
            return 1 * (1 + GetTotalAttributes().Strength / 100);
        }

        public override void LevelUp()
        {
            level++;
            attributes.Strength += 3;
            attributes.Dexterity += 2;
            attributes.Intelligence += 1;
        }
    }
}
