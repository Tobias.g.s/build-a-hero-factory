﻿using MyHero.Models.Equipments;
using MyHero.Models.Heros;
using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHero.Models.Heros.Classes
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(
            name,
            new List<ArmorTypes>() { ArmorTypes.Leather, ArmorTypes.Mail },
            new List<WeaponTypes>() { WeaponTypes.Dagger, WeaponTypes.Sword }
            )
        {
            level = 1;
            attributes = new Utils.Attributes(2, 6, 1);
        }
        public override void LevelUp()
        {
            attributes.Strength += 1;
            attributes.Dexterity += 4;
            attributes.Intelligence += 1;
        }
        public override double DoDamage()
        {
            Attributes attributes = GetTotalAttributes();
            Weapon? weapon = (Weapon?)Equipped[Slots.Weapon];
            if (weapon != null)
            {
                return weapon.WeaponDam * (1 + ((attributes.Dexterity + attributes.Strength) / 100));
            }
            return 1 * (1 + (attributes.Dexterity + attributes.Strength) / 100);
        }
    }
}
