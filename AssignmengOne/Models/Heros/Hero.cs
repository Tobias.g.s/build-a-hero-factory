﻿using MyHero.Models.Equipments;
using MyHero.Models.Exceptions;
using MyHero.Utils;
using System.Globalization;
using System.Text;

namespace MyHero.Models.Heros
{
    /// <summary>
    /// A mighty hero, used to create heroes of varius calibur.
    /// </summary>
    public abstract class Hero
    {
        public string name;
        public int level;
        public Attributes attributes;
        // total attributes from all equipments.
        public Attributes TotalAttributes { get { return GetTotalAttributes(); }}
        public double Armor { get { return GetTotalArmor(); } }
        public double Damage { get { return DoDamage(); } }
        public  List<ArmorTypes> ArmorSchool { get; set; }
        public  List<WeaponTypes> WeaponSchool { get; set; }
        public IDictionary<Slots, Equipment?> Equipped= new Dictionary<Slots, Equipment?>();
        public Hero(string name, List<ArmorTypes> armorSchool, List<WeaponTypes> weaponSchool)
        {
            Equipped.Add(Slots.Weapon, null);
            Equipped.Add(Slots.Head, null);
            Equipped.Add(Slots.Body, null);
            Equipped.Add(Slots.Legs, null);
            this.name = name;
            this.ArmorSchool = armorSchool;
            this.WeaponSchool = weaponSchool;
        }
        public Attributes GetTotalAttributes()
        {
            Attributes a = attributes;
            foreach (var item in Equipped)
            {
                if (item.Value!=null)
                {
                    a += item.Value.Attributes;
                }
            }
            return a;
        }
        private double GetTotalArmor()
        {
            double a = 0;
            foreach (var item in Equipped)
            {
                if (item.Value != null && item.Key != Slots.Weapon)
                {
                    a += ((ArmorPiece)item.Value).ArmorValue;
                }
            }
            return a;
        }
        /// <summary>
        /// Equips an item of type Equipment with required level less or equal to the hero's current level.
        /// Item must be of correct level and school in order to be equipped.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="ArgumentException"> thrown when item level is higher then the hero's current level.</exception>
        public void Equip(Equipment item)
        {
            if(item.Slot == Slots.Weapon)
            {
                if (item.RequiredLevel > this.level)
                {
                    throw new InvalidWeaponException("The required level of this item is higher then your level. git gud.");
                }
                if (!WeaponSchool.Contains( ((Weapon)item).WeaponType)  )
                {
                    throw new InvalidWeaponException("The class is not profficient in a weapon of given type.");
                }
            }
            else
            {
                if (item.RequiredLevel > this.level)
                {
                    throw new InvalidArmorException("The required level of this item is higher then your level. git gud.");
                }
                if (!ArmorSchool.Contains(((ArmorPiece)item).ArmorType))
                {
                    throw new InvalidArmorException("The class is not profficient in a Armor of given type.");
                }
            }
            this.Equipped[item.Slot] = item;
            //switch (item.Slot)
            //{
            //    case (Slots.Weapon):
            //        this.Equipped[0] = new KeyValuePair<Slots, Equipment?>(Slots.Weapon, item);
            //        break;
            //    case (Slots.Head):
            //        this.Equipped[1] = new KeyValuePair<Slots, Equipment?>(Slots.Head, item);
            //        break;
            //    case (Slots.Body):
            //        this.Equipped[2] = new KeyValuePair<Slots, Equipment?>(Slots.Body, item);
            //        break;
            //    case(Slots.Legs):
            //        this.Equipped[3] = new KeyValuePair<Slots, Equipment?>(Slots.Legs, item);
            //        break;
            //}
        }
        /// <summary>
        /// Displays Hero name, class attribute, equipment and armor.
        /// </summary>
        public void DisplayHero()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Name: ");
            sb.Append(name);
            sb.Append("\nLevel:  ");
            sb.Append(level);
            sb.Append("\nClass: ");
            sb.Append(GetType().Name);
            sb.Append("\nAttributes : ");
            sb.Append(TotalAttributes);
            sb.Append("\nArmor :");
            sb.Append(Armor);
            string items = "\n";
            foreach (var item in Equipped)
            {
                if (item.Value!= null)
                {
                    items += "\n"+ item.Value.Slot.ToString() + ": " + item.Value.Name;
                }
            }
            sb.Append(items);
            Console.WriteLine(sb.ToString());
        }
        /// <summary>
        /// Increases level and Increments the attributes of the hero based on class.
        /// </summary>
        public abstract void LevelUp();
        /// <summary>
        /// Caclulates damage based on hero's attributes, equipement and class.
        /// </summary>
        /// <returns> Double</returns>
        public abstract double DoDamage();
    }
}
