# Assignment One

This project was done as an assignment for Noroff Accelerate to demonstrate basic c# and OOP knowledge.

## Installation
Nothing special to intall this other than
```bash
	Git clone https://gitlab.com/Tobias.g.s/build-a-hero-factory.git
```
## Description
This application lets users create Heros and items of varius types.
There is by default four types of heros reffered to as classes.
##Classes:
Mage  
Warrior  
Rogue  
Ranger

Each Hero class has a set of three attributes : Strength , Intelligence and Dexterity.
Each class favors one or two attributes which in turn is used to calculate damage
, this is expressed through how the attributes are divided during levling.

Each Hero can level up, levling up results in an increment in the characters level as well as its attributes.
The level up method will increment the heroes attributes a set amount reflecting its main attributes.
Warriors gain strength, mages gain intelligence, rangers gain dexterity and rogues gain both strenght and dex.

Each Hero has a damage attribute where damage is calculated based on the total of all attributes across the hero itself as well as all equipments the hero may have equipped.
``` c#
	public override double DoDamage()
    {
        Weapon? weapon = (Weapon?)Equipped[Slots.Weapon];
        if (weapon != null)
        {
            return weapon.WeaponDam * (1 + (GetTotalAttributes().Intelligence / 100));
        }
        return 1 * (1 + GetTotalAttributes().Intelligence / 100);
    }
```
## Equipment
Equipments are items a hero can equip given the hero class is proficient in its type as well as the level requirement is met.

  Equipments can be of two main types :Armor and Weapons, which are in turn further divided into weaponSchools and ArmorSchools.
### ArmorSchools:
Mail  
leather  
cloth  
plate  
### WeaponSchools:
Staff  
Sword  
Axe  
Hammer  
Dagger  
Bow  

Equipments themselves have their own attribute scores which are added to get the hero's total attributes.
The Weapon is used both for its attributes as well to calculate damage.

  A hero has four main slots where equippments can be equipped: Weapon, Head, Body, Legs.
