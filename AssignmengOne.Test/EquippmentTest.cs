﻿using MyHero.Models.Equipments;
using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmengOne.Test
{
    public class EquippmentTest
    {
        //    [Fact]
        //    private void Make_Equippment_ShouldHaveNameAs_Helmet()
        //    {
        //        // Arrange
        //        string expected = "Helmet";
        //        // Act
        //        Equipment equipment = new Equipment("Helmet", 1, Slots.Head);
        //        string actual = equipment.Name;
        //        // Assert
        //        Assert.Equal(expected, actual);
        //    }

        //    [Fact]
        //    private void Make_Equippment_ShouldHaveSlotAs_Head()
        //    {
        //        // Arrange
        //        Slots expected = Slots.Head;
        //        // Act
        //        Equipment equipment = new Equipment("Helmet", 1, Slots.Head);
        //        Slots actual = equipment.Slot;
        //        // Assert
        //        Assert.Equal(expected, actual);
        //    }
        [Fact]
        private void Make_Weapon_Should_Have_Name()
        {
            // Arrange
            string expected = "Sword of Pastrami";

            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            Weapon SoP = new Weapon("Sword of Pastrami", 1, attributes,1.0, WeaponTypes.Sword);
            string actual = SoP.Name;
            
            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        private void Make_Weapon_Should_Have_TypeAsWeapon()
        {
            // Arrange
            Slots expected = Slots.Weapon;
            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            Weapon SoP = new Weapon("Sword of Pastrami", 1, attributes, 1.0, WeaponTypes.Sword);
            Slots actual = SoP.Slot;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        private void Make_Weapon_Should_Have_WeaponTypeSword()
        {
            // Arrange
            WeaponTypes expected = WeaponTypes.Sword;
            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            Weapon SoP = new Weapon("Sword of Pastrami", 1, attributes, 1.0, WeaponTypes.Sword);
            WeaponTypes actual = SoP.WeaponType;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        private void Make_Weapon_Should_Have_LevelRequirementOne()
        {
            // Arrange
            int expected = 1;
            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            Weapon SoP = new Weapon("Sword of Pastrami", 1, attributes, 1.0, WeaponTypes.Sword);
            int actual = SoP.RequiredLevel;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        private void Make_Weapon_Should_Have_WeaponDamOne()
        {
            // Arrange
            double expected = 1.0;
            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            Weapon SoP = new Weapon("Sword of Pastrami", 1, attributes, 1.0, WeaponTypes.Sword);
            double actual = SoP.WeaponDam;
            // Assert
            Assert.Equal(expected, actual);
        }

        // armor tests
        [Fact]
        private void Make_ArmorPiece_Should_Have_Name()
        {
            // Arrange
            string expected = "Helmet of Bagel";
            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            ArmorPiece HoB = new ArmorPiece("Helmet of Bagel", 1, Slots.Head, attributes, ArmorTypes.Plate, 1.0);
            string actual = HoB.Name;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        private void Make_ArmorPiece_Should_Have_Slot()
        {
            // Arrange
            Slots expected = Slots.Head;
            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            ArmorPiece HoB = new ArmorPiece("Helmet of Bagel", 1, Slots.Head, attributes, ArmorTypes.Plate, 1.0);
            Slots actual = HoB.Slot;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        private void Make_ArmorPiece_Should_ThrowExcetion_WhenSlotIsWeapon()
        {
            // Act&Assert
            Attributes attributes = new Attributes(0, 0, 0);
            Assert.Throws<ArgumentException>(()=>new ArmorPiece("Helmet of Bagel", 1, Slots.Weapon, attributes, ArmorTypes.Plate, 1.0));
        }

        [Fact]
        private void Make_ArmorPiece_Should_have_Attributes()
        {
            // Arrange
            Attributes expected = new Attributes(0, 0, 0);
            // Act
            Attributes attributes = new Attributes(0, 0, 0);
            ArmorPiece HoB = new ArmorPiece("Helmet of Bagel", 1, Slots.Head, attributes, ArmorTypes.Plate, 1.0);
            Attributes actual = HoB.Attributes;

            // Assert
            Assert.Equal(expected, actual);

        }

    }
}
