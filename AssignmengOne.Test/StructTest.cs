﻿using MyHero.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmengOne.Test
{
    public class StructTest
    {
        [Fact]
        private void Add_Two_Attributes_ShouldReturn_NewAttribute_WhereAllStatsAreAddedTogetherBasedOnTypeOfStat()
        {
            // Arrange
            Attributes expected = new Attributes(2, 4, 6);
            // Act
            Attributes a = new Attributes(1, 2, 3);
            Attributes b = new Attributes(1, 2, 3);
            Attributes actual = a + b;
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
