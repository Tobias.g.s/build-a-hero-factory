using MyHero.Models.Equipments;
using MyHero.Models.Heros.Classes;
using MyHero.Models.Heros;
using MyHero.Utils;
using MyHero.Models.Exceptions;

namespace MyHero.Test
{
    public class HeroTest
    {
        [Fact]
        public void Make_MakeWarrior_ShouldBeLevelOne()
        {
            // Arrange
            int expected = 1;
            // Act
            Warrior bob = new Warrior("bob");
            int actual = bob.level;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Make_MakeWarrior_NameShouldBeEqualToInput()
        {
            // Arrange
            string expected = "bob";
            // Act
            Warrior bob = new Warrior("bob");
            string actual = bob.name;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Make_MakeWarrior_ShouldHaveWarriorStartStats()
        {
            // Arrange
            Attributes expected = new Attributes(5,2,1);
            // Act
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            Attributes actual = bob.attributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void Make_MakeWarrior_ShouldHaveWarriorArmorSchool()
        {
            // Arrange
            List<ArmorTypes> expected = new List<ArmorTypes>() { ArmorTypes.Plate, ArmorTypes.Mail };
            // Act
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            List<ArmorTypes> actual = bob.ArmorSchool;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void Make_MakeWarrior_ShouldHaveWarriorWeaponSchool()
        {
            // Arrange
            List<WeaponTypes> expected = new List<WeaponTypes>() 
            { 
                WeaponTypes.Axe, 
                WeaponTypes.Hammer, 
                WeaponTypes.Sword 
            };

            // Act
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            List<WeaponTypes> actual = bob.WeaponSchool;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void Level_LevelUpWarrior_ShouldBeLevelTwo()
        {
            // Arrange
            int expected = 2;
            // Act
            Warrior bob = new Warrior("bob");
            bob.LevelUp();
            int actual = bob.level;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Level_LevelUpWarrior_StatsShouldBeIncrementedCorrectly()
        {
            // Arrange
            Attributes expected = new Attributes(8, 4, 2);
            // Act
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            bob.LevelUp();
            Attributes actual = bob.attributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void Make_MakeNewHero_Should_Have_All_EquippmentSlots_As_Null()
        {
            // Arrange
            Equipment? expected = null;
            // Act 
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            Equipment? actual = bob.Equipped[Slots.Head];
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void Get_GetHero_TotalAttributesWithNoItemsEquipped_ShouldReturnBaseAttributes()
        {
            // Arrange
            Attributes expected = new Attributes(5, 2, 1);
            // Act
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            Attributes actual = bob.TotalAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void GetTotalAttributes_WithItemsEquipped_ShouldReturnBaseAttributesPlusEquipmentAttributes()
        {
            // Arrange
            Attributes expected = new Attributes(6, 3, 2);
            // Act
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            ArmorPiece Helm = new ArmorPiece(
                "Helmet of dud stats", 
                1, 
                Slots.Head, 
                new Attributes(1, 1, 1), 
                ArmorTypes.Plate,
                10
                );
            bob.Equip(Helm);
            Attributes actual = bob.TotalAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void Epuip_HeroEquipAnEquipment_EquipmentShouldBeInCorrectSlot()
        {
            // Arrange
            Weapon SoP = new Weapon("Sword of Provelone", 1, new Attributes(2, 0, 0), 5.0, WeaponTypes.Sword);
            Equipment expected = SoP;
            // Act
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            bob.Equip(SoP);
            Equipment? actual = bob.Equipped[Slots.Weapon];
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void Equip_EquipWeaponWithRequiredLevelHigherThanHeroLevel_ShouldThrowError()
        {
            // Arrange
            Weapon SoP = new Weapon("Sword of Provelone", 2, new Attributes(2, 0, 0), 5.0, WeaponTypes.Sword);
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => bob.Equip(SoP));
        }
        [Fact]
        public void Equip_EquipWeaponWithWrongWeaponType_ShouldThrowError()
        {
            // Arrange
            Weapon SoP = new Weapon("Staff of Provelone", 2, new Attributes(1, 0, 0), 5.0, WeaponTypes.Staff);
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => bob.Equip(SoP));
        }

        [Fact]
        public void Equip_EquipArmorWithRequiredLevelHigherThanHeroLevel_ShouldThrowError()
        {
            // Arrange
            ArmorPiece HoP = new ArmorPiece("Helm of Provelone", 2, Slots.Head, new Attributes(1, 0, 0), ArmorTypes.Plate, 5.0);
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => bob.Equip(HoP));
        }
        [Fact]
        public void Equip_EquipArmorWithWrongAmorType_ShouldThrowError()
        {
            // Arrange
            ArmorPiece HoP = new ArmorPiece("Helm of Provelone", 1, Slots.Head,new Attributes(1, 0, 0), ArmorTypes.Cloth, 5.0);
            Warrior bob = new Models.Heros.Classes.Warrior("bob");
            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => bob.Equip(HoP));
        }

        [Fact]
        public void DoDamageWarrior_DoDamageWithNoWeapon_ShouldReturn_Return_OnePlusStrength()
        {
            // Arrange
            double expected = 1 * (1 + 5 / 100);
            // Act
            Warrior Bobby = new Warrior("Bobby");
            double actual = Bobby.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DoDamageRogue_DoDamageWithNoWeapon_ShouldReturn_Return_OnePlusStrengthAndDex()
        {
            // Arrange
            double expected = 1 * (1 + (2+6) / 100);
            // Act
            Rogue Bobby = new Rogue("Bobby");
            double actual = Bobby.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DoDamageWarrior_DoDamageWithWeapon_ShouldReturn_Return_WeaponDamPlusStrength()
        {
            // Arrange
            double expected = 2 * (1 + 5 / 100);
            // Act
            Weapon SoS = new Weapon("Sword of Sphagett", 1, new Attributes(0, 0, 0), 2.0, WeaponTypes.Sword);
            Warrior Bobby = new Warrior("Bobby");
            Bobby.Equip(SoS);
            double actual = Bobby.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipSecondWeapon_NewWeaponShouldBeEquipped()
        {
            // Arrange 
            Weapon expected = new Weapon("Sword of Sphagett", 1, new Attributes(0, 0, 0), 2.0, WeaponTypes.Sword);
            // Act 
            Weapon Sword = new  Weapon("Sword of Sphagett", 1, new Attributes(0, 0, 0), 2.0, WeaponTypes.Sword);
            Weapon Axe = new  Weapon("Axe of Omlettes", 1, new Attributes(0, 0, 0), 2.0, WeaponTypes.Axe);
            Warrior Bobby = new Warrior("Bobby");
            Bobby.Equip(Axe);
            Bobby.Equip(Sword);
            Equipment? actual = Bobby.Equipped[Slots.Weapon];
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void DoDamageWarrior_DoDamageWithWeaponWithReEqpuippedWeapon_ShouldReturn_Return_NewWeaponDamPlusStrength()
        {
            // Arrange
            double expected = 3 * (1 + 5 / 100);
            // Act
            Weapon SoS = new Weapon("Sword of Sphagett", 1, new Attributes(0, 0, 0), 2.0, WeaponTypes.Sword);
            Weapon AoS = new Weapon("Axe of Slime", 1, new Attributes(0, 0, 0), 3.0, WeaponTypes.Axe);
            Warrior Bobby = new Warrior("Bobby");
            Bobby.Equip(SoS);
            Bobby.Equip(AoS);
            double actual = Bobby.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void GetTotalArmor_WithNoItemsEquipped_ShouldReturnZero()
        {
            // Arrange
            double Expected = 0;
            // Act
            Mage glassCannonSteve = new Mage("Stephen");
            double actual = glassCannonSteve.Armor;
            // Assert
            Assert.Equal(Expected, actual);
        }
        [Fact]
        public void GetTotalArmor_WithItemsEquipped_ShouldReturnTen()
        {
            // Arrange
            double Expected = 10;
            // Act
            Mage gigaChadStephano = new Mage("Stephen");
            ArmorPiece cloak = new ArmorPiece("Common Cloak", 1, Slots.Body, new Attributes(0, 0, 0), ArmorTypes.Cloth, 10);
            gigaChadStephano.Equip(cloak);
            double actual = gigaChadStephano.Armor;
            // Assert
            Assert.Equal(Expected, actual);
        }
    }
}